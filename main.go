package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"

	rotatelogs "github.com/lestrrat/go-file-rotatelogs"
)

type logWriter struct {
}

func (writer logWriter) Write(bytes []byte) (int, error) {
	return fmt.Print(time.Now().UTC().Format("2006-01-02T15:04:05.999Z") + " [DEBUG] " + string(bytes))
}

func initiLogger(path string, rotate, age int) {

	writer, err := rotatelogs.New(
		//fmt.Sprintf("%s.%s", path, "%Y%m%d.%H%M%S"),
		fmt.Sprintf("%s.%s", path, "%Y%m%d.%H%M%S"),
		//rotatelogs.WithLinkName("/var/log/service/current"),
		rotatelogs.WithMaxAge(time.Hour*time.Duration(age)),
		rotatelogs.WithRotationTime(time.Second*time.Duration(rotate)),
	)

	if err != nil {
		log.Fatalf("Failed to Initialize Log File %s", err)
	}
	log.SetOutput(writer)
	return
}
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	path := os.Getenv("path")
	total := os.Getenv("total")
	rotate := os.Getenv("rotate")
	age := os.Getenv("age")
	raw := os.Getenv("raw")

	if total == "" {
		total = "0"
	}
	if rotate == "" {
		rotate = "60"
	}
	if age == "" {
		age = "7"
	}
	if path == "" {
		path = "tdr"
	}
	if raw == "" {
		raw = "raw"
	}
	dat, err := ioutil.ReadFile("raw")
	check(err)

	totalx, _ := strconv.Atoi(total)
	rotatex, _ := strconv.Atoi(rotate)
	agex, _ := strconv.Atoi(age)

	initiLogger(path, rotatex, agex)
	for i := 0; i < totalx; i++ {
		//time.Sleep(time.Second * 1)
		log.Printf(string(dat) + "\n")
	}
	//fmt.Scanln()
}
